# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kparts
pkgver=5.58.0
pkgrel=1
pkgdesc="Document centric plugin system"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends_dev="kio-dev kiconthemes-dev ktextwidgets-dev sonnet-dev kbookmarks-dev kwidgetsaddons-dev kcompletion-dev kitemviews-dev kjobwidgets-dev solid-dev kxmlgui-dev kconfigwidgets-dev kauth-dev kcodecs-dev kconfig-dev kcoreaddons-dev ki18n-dev kservice-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring running X11

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="3b932af859a6e34538949ce7bb7c17bbf2d8e7ee7cc0c144180c5f36a3dad28b9b26b9ebf7ae08f6e586d24e066da17f3edd6fee096577151b386f327faeb70b  kparts-5.58.0.tar.xz"
